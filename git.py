# -*- coding: utf-8 -*-
import shlex
import subprocess

class Log:
    def __init__(self, line):
        self.line = line
        self.sha, self.author, self.message = line.split(';',maxsplit=2)
    def __repr__(self):
        return self.line
    @classmethod
    def map_stream(cls, stream):
        return [cls(l) for l in stream.decode('utf-8').splitlines()]

def git_decode(letter):
    code = {
            ' ': 'unmodified',
            'M': 'modified',
            'A': 'added',
            'D': 'deleted',
            'U': 'unmerged',
            '?': 'untracked'
            }
    return code.get(letter, None)

class PathStatus:
    """
    File status as output by git status --porcelain

    TODO: consider using porcelain=v2
    """

    def __init__(self, line, number):
        self.number = number
        self.line = line
        self.index = git_decode(line[0])
        self.tree = git_decode(line[1])
        self.path = line[3:]
    def __repr__(self):
        return self.line

class Status(list):
    #states = [ ('changed', 'modifiés non publiés'), ('saved', 'validés pour publication') ]
    #actions = [ ('save', 'valider'), ('skip', 'ignorer'), ('discard', 'abandonner') ]
    states = [ ('changed', 'modifiés non publiés') ]
    actions = [ ('save', 'publier'), ('skip', 'ignorer'), ('discard', 'abandonner') ]
    def __init__(self, gitstatus):
        self.source = gitstatus
        lines = gitstatus.decode('utf-8').splitlines()
        files = [ PathStatus(l,i) for i,l in enumerate(lines) ]
        super().__init__(files)

class Repo:
    cmd_list = []
    def __init__(self, path = None):
        self.path = path
        for cls, cmd in self.cmd_list:
            setattr(self, cmd, cls(self))

    @classmethod
    def command(this, name):
        def wrapper(cls):
            this.cmd_list.append((cls, name))
            return cls
        return wrapper

class GitCommand(object):
    """
    Base for git commands, to be instantiated
    in a git repo.
    """
    git_cmd = []
    def __init__(self, repo):
        self.repo = repo
    def git_args(self, *args, **kwargs):
        """
        convert args to actual git command arguments
        """
        return list(args)
    def before(self):
        """
        actions to be run before the git command
        (e.g. other git commands to be in a particular state)
        """
        pass
    def after(self):
        """
        actions to be run after the git command
        (e.g. to restore tree state)
        """
        pass
    def check_before(self):
        """
        checks to be done before
        """
        return True
    def result(self, rv):
        """
        postprocess the CompletedProcess value
        """
        return rv.returncode == 0
    def __call__(self, *args, **kwargs):
        self.before()
        assert self.check_before()
        args = self.git_cmd + self.git_args(*args)
        rv = subprocess.run(args, cwd=self.repo.path, check=True, stdout=subprocess.PIPE)
        self.after()
        return self.result(rv)

@Repo.command('noop')
class GitNoOp(GitCommand):
    pass

@Repo.command('status')
class GitStatus(GitCommand):
    git_cmd = ['git', 'status', '--porcelain']
    def result(self, rv):
        return Status(rv.stdout)

@Repo.command('log')
class GitLog(GitCommand):
    git_cmd = ['git', 'log', '--pretty="%h;%an;%s"']
    def result(self, rv):
        return Log.map_stream(rv.stdout)

@Repo.command('show')
class GitShow(GitCommand):
    git_cmd = ['git', 'show']
    def result(self, rv):
        return rv.stdout.decode('utf-8')

@Repo.command('diff')
class GitDiff(GitCommand):
    git_cmd = ['git', 'diff']
    def git_args(self, *args, **kwargs):
        if 'path' in kwargs:
            return ['--', kwargs.get('path')]
        return list(args)
    def result(self, rv):
        return rv.stdout.decode('utf-8')

@Repo.command('add')
class GitAdd(GitCommand):
    git_cmd = ['git', 'add']

@Repo.command('add_tree')
class GitAddTree(GitCommand):
    """
    adds all files in working directory,
    new and deleted ones
    """
    git_cmd = ['git', 'add', '--all']

@Repo.command('commit')
class GitCommit(GitCommand):
    git_cmd = ['git', 'commit']
    def git_args(self, message, author):
        author = shlex.quote(author)
        message = shlex.quote(message)
        return ['-m', message, '--author=%s'%(author)]

@Repo.command('commit_index')
class GitCommitIndex(GitCommit):
    pass

@Repo.command('commit_tree')
class GitCommitTree(GitCommit):
    git_cmd = ['git', 'commit']
    def check_before(self):
        return self.repo.add_tree()

#class GitCheckUpstream(GitCommand):
#    """
#    git fetch origin master
#    git merge-tree `git merge-base FETCH_HEAD master` FETCH_HEAD master
#    """
#    branch = 'master'
#    origin = 'origin'
#    class GitMergeBase(GitCommand):
#        git_cmd = ['git', 'merge-base', 'FETCH_HEAD', branch]
#    class GitMergeTree(GitCommand):
#        git_cmd = ['git', 'merge-tree']
#        def git_args(self, sha):
#            sha = merge_base()
#            return []
#    fetch = GitFetch()
#    merge_base = GitMergeBase()
#    merge_tree = GitMergeTree()
#    def __call__(self, *args):
#        self.fetch()
#        sha = self.merge_base()
#        return self.merge_tree(sha)

