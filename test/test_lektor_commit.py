import os
import pytest
import shutil
import tempfile
import json
import requests
import subprocess

from requests import Session
from urllib.parse import urljoin

@pytest.mark.base
def test_project(lektorproject):
    print(lektorproject)
    for (dirpath, dirnames, files) in os.walk(lektorproject):
        dirnames[:] = [ d for d in dirnames if d != '.git' ]
        #print(os.path.basename(dirpath))
        print(dirpath)
        print('/ '.join(dirnames), ' '.join(files))
    project = os.path.join(lektorproject,'test.lektorproject')
    assert os.path.exists(project), "project file"
    for package in ['lektor-admin-extra', 'lektor-login', 'lektor-commit']:
        filename = package.replace('-','_') + '.py'
        path = os.path.join(lektorproject,'packages',package,filename)
        assert os.path.exists(path), "missing file %s"%path

@pytest.mark.base
def test_git_repo(lektorproject):
    git = os.path.join(lektorproject,'.git')
    assert os.path.exists(git), "git repo"

@pytest.mark.server
def test_server_started(anonymous):
    rv = anonymous.get('/', timeout=0.1)
    assert rv.status_code == 200

@pytest.mark.server
def test_auth_buttons(anonymous):
    rv = anonymous.get('/', timeout=0.1)
    assert 'auth-button-div' in rv.text

@pytest.mark.server
def test_status_page(anonymous):
    rv = anonymous.get('/admin-git/status', timeout=0.1)
    assert '<body>' in rv.text
