import pytest

@pytest.mark.base
def test_status(git, gitmodif):
    rv = git.status()
    assert '?? tmp' in str(rv)

@pytest.mark.base
def test_git_session(git, gitmodif):

    print("[1] add tmp folder")
    rv = git.add('tmp')
    assert rv, "[1] add two configs"
    rv = git.status()
    assert 'A  tmp/toto.txt' in str(rv), "[1] after add"

    print("[2] commit this addition")
    rv = git.commit('add tmp', 'pytest <dev@null>')
    assert rv, "[2] commit"
    rv = git.status()
    assert 'tmp' not in str(rv), "[2] after commit"

    print("[3] commit untracked files")
    rv = git.commit_tree('add all files', 'pytest <dev@null>')
    assert rv, "[3] commit all tree"
    rv = git.status()
    assert len(rv) == 0, "[3] after commit all"

    print("[4] show log")
    rv = git.log()
    print(rv)
    assert len(rv) > 2, "[4] show log"
