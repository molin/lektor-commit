# -*- coding: utf-8 -*-
import subprocess
from flask import Blueprint, \
                  current_app, \
                  render_template, \
                  url_for, \
                  Markup, \
                  request, \
                  session
from lektor.pluginsystem import Plugin, get_plugin
from lektor.publisher import Publisher
from lektor.admin.modules import serve
from git import Repo

gitbp = Blueprint('git', __name__,
                           url_prefix='/admin-git',
                           static_folder='static',
                           template_folder='templates'
                           )
repo = Repo()

#@gitbp.route('/add',methods=['POST'])
#def add():
#    path = request.values.get('path', None)
#    if path:
#        rv = git_add(path)
#        return "done"
#    return "failed"
#
#@gitbp.route('/commit',methods=['POST'])
#def commit():
#    #author = request.values.get(
#    rv = git_commit()
#    #subprocess.call(['git', 'commit', '--author="John Doe <john@doe.org>"'])
#    return "failed"

class CommitPublisher(Publisher):
    """
    commit all changes from current tree
    get username from session cookie
    """
    _plugin = None
    @property
    def plugin(self):
        if self._plugin is None:
            self._plugin = get_plugin('commit', self.env)
        return self._plugin
    def get_authormail(self):
        user = self.plugin.current_author
        return '%s <%s>'%(user.get('username'),user.get('email'))
    def git_process(self):
        yield 'done'
    def publish(self, target_url, credentials=None, **extra):
        try:
            yield from self.git_process(target_url)
        except subprocess.CalledProcessError as e:
            self.fail("erreur lors du bilan des modifications %s"%e)

class AskReviewPublisher(CommitPublisher):
    """
    commit changes and ask for review before push
    """
    def send_mail(self, address, author, data):
        subject = "[Review] site modifié par %s"%author
        sender = self.plugin.mail_info.get('sender')
        reply_to = "replyto=%s"%self.plugin.mail_info.get('reply_to')
        cc = author
        body = status.source
        subprocess.run(['mail','-s',subject,'-r',sender,'-c',cc,'-S',replyto,address], input=body)
    def git_process(self, target_url):
        repo.add_tree() or self.fail("erreur lors du bilan des modifications")
        status = repo.status()
        if len(status) == 0:
            self.fail('aucune modification à signaler')
        author = self.get_authormail()
        yield '%d fichiers modifiés par %s'%(len(status), author)
        for s in status:
            yield str(s)
        repo.commit('interface commit', author) or self.fail("erreur lors de la sauvegarde")
        self.send_mail('Pascal <molin.maths@gmail.com>', author, status)
        yield 'Message envoyé'
        yield 'Les changements seront publiés après validation.'

class GitPushPublisher(CommitPublisher):
    """
    commit modified files and push

    caveat: the publish method does not have access to the request
    which triggered it, hence the publisher name.
    As a tentative workaround, we must store the user associated
    to the last valid /publish request
    """
    def publish(self, target_url, credentials=None, **extra):
        #src_path = self.output_path
        #dst_path = target_url.path
        #self.fail("Vous n'avez pas le droit de publier directement, \
        #        sélectionnez plutôt «signaler les changements pour publication»")
        app = current_app
        login = get_plugin('login', self.env)
        with app.app_context():
            user = login.get_auth_user()
            if user is not None and user.get('publish',False):
                msg = 'user %s'%user['username']
                yield msg
                yield 'Done'
        self.fail('Please commit all changes before publishing')

class CommitPlugin(Plugin):
    name = 'lektor-commit'
    description = u'Publish by git commit+push, or ask review.'

    def on_setup_env(self, *args, **extra):

        repo.path = self.env.root_path

        admin_extra = get_plugin('admin-extra', self.env)
        login = get_plugin('login', self.env)

        self.current_author = None

        config = self.get_config()
        self.mail_info = {
                'sender': config.get('mail.sender','<no-reply@dev.null>'),
                'reply_to': config.get('mail.reply_to','<no-reply@dev.null>'),
                }

        self.env.add_publisher('askreview', AskReviewPublisher)
        self.env.add_publisher('gitpush', GitPushPublisher)

        @serve.bp.before_app_first_request
        #pylint: disable=unused-variable
        def setup_git_bp():

            app = current_app
            app.register_blueprint(gitbp)

            url = url_for('git.status')
            svg = url_for('git.static',filename='git.svg')
            admin_extra.add_button( url,
                    'see all changes',
                    Markup('<img src="%s" alt="git status">'%svg))


        @gitbp.route('/status',methods=['GET'])
        @login.access()
        #pylint: disable=unused-variable
        def status():
            return render_template("status.html", this = repo.status())

        @gitbp.route('/log',methods=['GET'])
        @login.access(level='admin')
        #pylint: disable=unused-variable
        def log():
            return render_template("log.html", this = repo.log())

        @gitbp.route('/show',methods=['GET'])
        @login.access()
        #pylint: disable=unused-variable
        def show():
            sha = request.args.get('sha')
            return render_template("show.html", this = repo.show(sha))

        @gitbp.route('/diff',methods=['GET'])
        @login.access()
        #pylint: disable=unused-variable
        def diff():
            path = request.args.get('path','')
            return render_template("diff.html", this = repo.diff(path=path))

        ## use to add/commit after each use
        #@api.bp.after_request
        ##pylint: disable=unused-variable
        #def after_request_api(response):
        #    # add modification to user cookie
        #    # can use request
        #    return response

    def on_login_access_api(self, request=None, user=None, **kwargs):
        # could perform git add / git commit here
        if request and request.path == '/admin/api/publish':
            self.current_author = user
