# lektor-commit

Add minimal `git` facilities to lektor development server.

This plugin requires ``lektor-auth`` to obtain username.

## Commit, Push

By default, a ``commit`` includes the entire directory state, adding 
untracked files and removing deleted ones. In git language:
```
git add -u :/
git commit --author=<authuser> -m 'interface commit, user <username>'
```

A ``push`` can be done to a remote server if there is no conflict.
following https://stackoverflow.com/questions/501407/is-there-a-git-merge-dry-run-option
it first checks there is no merge conflict
```
git fetch origin master
git merge-tree `git merge-base FETCH_HEAD master` FETCH_HEAD master
```
and eventually runs
```
git pull origin master
git push origin master
```

## Publishing methods

Two publishing methods are added:

- ``askreview`` performs a commit and then sends an email to ask for review.
  The email address has to be configured in ``configs/commit.ini``
  ```
  [askreview]
  name = reviewer
  email = moderator@review.com 
  ```

- ``gitpush`` does the commit and if possible pushes to server.
  ```
  [gitpush]
  server = git://remote.com/lektor-repo
  ```

## Further configuration

The following is not yet available and only partially implemented.

When a review is requested, the commit should consist only
in files modified by the user, logged for example in the user cookie.

A push should never generate a conflict.

The plugin provides a status page, giving access to manual commit of selected
files.
